#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <chrono>
#include <ctime>  
using namespace std;

int menu();
//Declarations for City
void viewCities();
//Declarations for Hotel
void viewHotels();
bool wantToBook();
void viewBookings();
//Declarations for Booking
struct Booking;
bool isValidDate(string date);
int daysBetween(string checkIn, string checkOut);
bool checkOutAfterCheckIn(string checkIn, string checkOut);
void addBooking(string fileName);
void deleteBooking(string fileName);
void viewBookings(string fileName);

// LAST MINUTE SHIFTING MEANT THAT RATHER THAN HAVE 3 FILES OF HOTEL.H CITY.H AND BOOKING.H I HAVE THIS ONE FILE
// I DO NOT LIKE HOW LONG IT IS, I LIKED THE PREVIOUS HIERARCHY BUT ITS FUNCTIONAL AND LESS INTERDEPENDENT

int menu(){
        string fileName = "bookings.txt";
    int choice;

    while (true) {
    cout << "-----------------------" << endl;
    cout << "----Booking Service----" << endl;
    cout << "-----------------------" << endl;
    cout << "1. Browse Cities" << endl;
    cout << "2. Browse Hotels" << endl;
    cout << "3. Add custom booking" << endl;
    cout << "4. View all bookings" << endl;
    cout << "5. Delete booking" << endl;
    cout << "6. Exit" << endl;
    cout << "-----------------------" << endl;
    cout << "Enter your choice: ";

    string choiceStr;
    getline(cin, choiceStr);

    if (all_of(choiceStr.begin(), choiceStr.end(), ::isdigit)) {
        int choice = stoi(choiceStr);
        if (choice >= 1 && choice <= 6) {
            if (choice == 1) {
                viewCities();//Browse cities
            } else if (choice == 2) {
                viewHotels();//Browse hotels
            } else if (choice == 3) {
                addBooking(fileName);//Add custom booking
            } else if (choice == 4) {
                viewBookings(fileName);//View booking    
            } else if (choice == 5) {
                viewBookings(fileName);//Delete booking
                deleteBooking(fileName);
            } else if (choice == 6) {
                cout << "Thank you for using this booking service :) " << endl;
                break;
            } else {
                break;
            }
        } else {
            cout << "Invalid choice. Please enter a valid number between 1 and 6." << endl;
        }
    } else {
        cout << "Invalid choice. Please enter a valid number." << endl;
    }
}
}

void addBooking(string fileName) {
    Booking booking;

    cout << "Enter user name: ";
    getline(cin, booking.userName);

    cout << "Enter hotel name: ";
    getline(cin, booking.hotelName);

    while(true) {
        cout << "Enter check-in date (YYYY-MM-DD): ";
        getline(cin, booking.checkInDate);

        if (isValidDate(booking.checkInDate)) {
            break;
        } else {
            cout << "Invalid date. Please enter a valid date in the format YYYY-MM-DD." << endl;
        }
    }

    while(true) {
        cout << "Enter check-out date (YYYY-MM-DD): ";
        getline(cin, booking.checkOutDate);

        if (isValidDate(booking.checkOutDate) && checkOutAfterCheckIn(booking.checkInDate, booking.checkOutDate)) {
            break;
        } else {
            cout << "Invalid date. Please enter a valid check-out date that is after the check-in date in the format YYYY-MM-DD." << endl;
        }
    }

    
    booking.stayDuration = to_string(daysBetween(booking.checkInDate, booking.checkOutDate));
    cout << "That is " << booking.stayDuration << " days" << endl;

    cout << "Enter price: ";
    getline(cin, booking.cpricePerNight);
    booking.stayPrice = to_string(stoi(booking.cpricePerNight) * stoi(booking.stayDuration)); //Reminder that this is custom booking
    cout << "The price for stay is " << booking.stayPrice << endl;

    ofstream outFile(fileName, ios::app);
            outFile << "User: "<< booking.userName <<" || Hotel: "<< booking.hotelName <<" || Checkin: "<< booking.checkInDate <<" || Checkout: "<< booking.checkOutDate <<" || Duration: "<< booking.stayDuration <<" days || Price: " << booking.stayPrice << endl;        
        outFile.close();

        cout << "Booking added!" << endl;
    }

/*
booking.h
-----------------------
Struct Booking 
bool isValidDate (string date)
daysBetween ()
checkOutAfterCheckIn
addBooking
deleteBooking
viewBookings
*/


// Calculate price for hotel booking with pricePerNight from Hotel class somehow import the selected hotel

struct Booking {
    string userName;
    string hotelName;
    string checkInDate;
    string checkOutDate;
    string stayDuration;
    string stayPrice;
    string cpricePerNight;//custom price per night
};

bool isValidDate(string date) {
    int year, month, day;
    if (sscanf(date.c_str(), "%d-%d-%d", &year, &month, &day) != 3) {
        return false;
    }
    tm t = { 0 };
    t.tm_year = year - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;

    time_t tt = mktime(&t);
    if (tt == -1) {
        return false;
    }
    return true;
}

int daysBetween(string checkIn, string checkOut) {
    int year1, month1, day1, year2, month2, day2;
    sscanf(checkIn.c_str(), "%d-%d-%d", &year1, &month1, &day1);
    sscanf(checkOut.c_str(), "%d-%d-%d", &year2, &month2, &day2);

    tm t1 = { 0 };
    t1.tm_year = year1 - 1900;
    t1.tm_mon = month1 - 1;
    t1.tm_mday = day1;
    tm t2 = { 0 };
    t2.tm_year = year2 - 1900;
    t2.tm_mon = month2 - 1;
    t2.tm_mday = day2;

    time_t tt1 = mktime(&t1);
    time_t tt2 = mktime(&t2);

    return (int)difftime(tt2, tt1) / (60 * 60 * 24);
}

bool checkOutAfterCheckIn(string checkIn, string checkOut) {
    int daysDiff = daysBetween(checkIn, checkOut);
    if (daysDiff > 0) {
        return true;
    } else {
        return false;
    }
}


//deleteBooking-- it still has room for improvement beause its savage in sensing any string similarity to delete rather than the exact match, check later
void deleteBooking(string fileName) {
    string userName;
    string hotelName;

    cout << "Enter user name: ";
    getline(cin, userName);

    bool entryFound = false;

    ifstream inFile(fileName);
    ofstream tempFile("temp.txt");

    string line;
    while (getline(inFile, line)) {
        if (line.find(userName) == string::npos) {
            tempFile << line << endl;
        }
        else {
            entryFound = true;
        }
    }

    inFile.close();
    tempFile.close();

    remove(fileName.c_str());
    rename("temp.txt", fileName.c_str());

    if (entryFound) {
        cout << "Booking deleted!" << endl;
    }
    else {
        cout << "No booking found with that user name. Please try again." << endl;
        deleteBooking(fileName);
    }
}

void viewBookings(string fileName) {
    ifstream inFile(fileName);
    string line;
    while (getline(inFile, line)) {
        cout << line << endl;
    }
    inFile.close();
}

/*
city.h
-------------
Class City
City objectsx25
void viewCities() 
*/

class City {
public:
    string cityName;

    City(string city) : cityName(city) {}

    void viewCity() {
        cout << "City: " << cityName << endl;
    }
};

City C1("New York");
City C2("Paris");
City C3("London");
City C4("Tokyo");
City C5("Sydney");


void viewCities() {
    cout << "1. New York" << endl;
    cout << "2. Paris" << endl;
    cout << "3. London" << endl;
    cout << "4. Tokyo" << endl;
    cout << "5. Sydney" << endl;
    cout << "6. Back to main menu" << endl;
    cout << "7. Exit" << endl;
    cout << "Enter your choice: ";

    string choiceStr;
    getline(cin, choiceStr);

    if (all_of(choiceStr.begin(), choiceStr.end(), ::isdigit)) {
        int choice = stoi(choiceStr);
        if (choice >= 1 && choice <= 7) {
            if (choice == 1) {
                cout << "-----------------------" << endl;
                C1.viewCity();
                cout << "-----------------------" << endl;
                //call the sub menu for the hotels in that city
            } else if (choice == 2) {
                cout << "-----------------------" << endl;
                C2.viewCity();
                cout << "-----------------------" << endl;
            } else if (choice == 3) {
                cout << "-----------------------" << endl;
                C3.viewCity();
                cout << "-----------------------" << endl;
            } else if (choice == 4) {
                cout << "-----------------------" << endl;
                C4.viewCity();
                cout << "-----------------------" << endl;
            } else if (choice == 5) {
                cout << "-----------------------" << endl;
                C5.viewCity();
                cout << "-----------------------" << endl;
            } else if (choice == 6) {
                menu();
            } else {
                exit(0);
            }
        } else {
            cout << "Invalid choice. Please enter a valid number between 1 and 7." << endl;
        }
    } else {
        cout << "Invalid choice. Please enter a valid number." << endl;
    }
}


/*
hotel.h
---------------------
Class Hotel
Hotel objectsx25
void viewHotelss() 
*/

//Quick Brainstorm for hotel and city merger, we can have a map, 
//or we can keep the objects separate since im not storing the city in booking

class Hotel {
private:
    string hotelName;
    int pricePerNight;
    string roomType;

public:
    Hotel(string name, int price, string type) {
        hotelName = name;
        pricePerNight = price;
        roomType = type;
    }

    string getHotelName() {
        return hotelName;
    }

    int getPricePerNight() {
        return pricePerNight;
    }

    string getRoomType() {
        return roomType;
    }

    int calculateStayPrice(int stayDuration) {
        return pricePerNight * stayDuration;
    }

    void aboutHotel(){
        cout << "-----------------------" << endl;
        cout << getHotelName() << endl;
        cout << "-----------------------" << endl;
        cout << "Price:" << getPricePerNight() << endl;
        cout << "Room Type:" << getRoomType() << endl;
        bool wantToBook();
    }

    bool wantToBook(){
        string choice;
        cout << "Do you wish to book this hotel? /N/Q)";
        cout << "1. Yes, book hotel" << endl;
        cout << "2. No, go back to main menu" << endl;
        cout << "3. Exit Program" << endl;
        cout << "Enter your choice: ";

        string choiceStr;
        getline(cin, choiceStr);

        if (all_of(choiceStr.begin(), choiceStr.end(), ::isdigit)) {
            int choice = stoi(choiceStr);
            if (choice >= 1 && choice <= 25) {
                if(choice == 1){
                    string fileName = "bookings.txt";
                    addBooking(fileName);
                    return true;
                }
                else if(choice == 2){
                    return false;
                }
                else if(choice == 3){
                    exit(0);
                } 
            } else {
                cout << "Invalid choice. Please enter a valid number between 1 and 25." << endl;
            } 
        } else {
            cout << "Invalid choice. Please enter a valid number." << endl;
        }
    }
};


// 5 Cities 5 Hotels

Hotel C1H1("The Grand Hotel", 100, "Deluxe");
Hotel C1H2("Evergrand Hotel", 30, "Basic");
Hotel C1H3("Barkeep Inn", 50, "Standard");
Hotel C1H4("Sunset Motel", 70, "Standard");
Hotel C1H5("Rising Sun Lodge", 70, "Basic");

Hotel C2H1("Seaside Resort", 200, "Ultra Deluxe");
Hotel C2H2("Beachside Inn", 40, "Basic");
Hotel C2H3("Duneview Hotel", 80, "Standard");
Hotel C2H4("Moonlight Motel", 60, "Standard");
Hotel C2H5("Rising Sun Lodge", 30, "Basic");

Hotel C3H1("Mountainpeak Retreat", 300, "Deluxe");
Hotel C3H2("Lakeside Lodge", 50, "Basic");
Hotel C3H3("Riverview Inn", 100, "Standard");
Hotel C3H4("Meadows Motel", 80, "Deluxe");
Hotel C3H5("Highland Haven", 90, "Basic");

Hotel C4H1("Garden Getaway", 400, "Ultra Deluxe");
Hotel C4H2("Forestside Inn", 60, "Basic");
Hotel C4H3("Riverside Resort", 90, "Standard");
Hotel C4H4("Valleyview Motel", 170, "Deluxe");
Hotel C4H5("Glenview Lodge", 25, "Basic");

Hotel C5H1("City Center Suites", 500, "Ultra Deluxe");
Hotel C5H2("Downtown Inn", 70, "Basic");
Hotel C5H3("Urbanview Hotel", 120, "Standard");
Hotel C5H4("Skyline Motel", 150, "Deluxe");
Hotel C5H5("Suburbia Haven", 45, "Basic");




void viewHotels() {
    cout << "1. The Grand Hotel" << endl;
    cout << "2. Evergrand Hotel" << endl;
    cout << "3. Barkeep Inn" << endl;
    cout << "4. Sunset Motel" << endl;
    cout << "5. Rising Sun Lodge" << endl;
    cout << "6. Seaside Resort" << endl;
    cout << "7. Beachside Inn" << endl;
    cout << "8. Duneview Hotel" << endl;
    cout << "9. Moonlight Motel" << endl;
    cout << "10. Mountainpeak Retreat" << endl;
    cout << "11. Lakeside Lodge" << endl;
    cout << "12. Riverview Inn" << endl;
    cout << "13. Meadows Motel" << endl;
    cout << "14. Garden Getaway" << endl;
    cout << "15. Forestside Inn" << endl;
    cout << "16. Riverside Resort" << endl;
    cout << "17. Valleyview Motel" << endl;
    cout << "18. City Center Suites" << endl;
    cout << "19. Downtown Inn" << endl;
    cout << "20. Urbanview Hotel" << endl;
    cout << "21. Skyline Motel" << endl;
    cout << "22. Suburbia Haven" << endl;
    cout << "23. Back to view cities" << endl;
    cout << "24. Back to main menu" << endl;
    cout << "25. Exit" << endl;
    cout << "Enter your choice: ";

    string choiceStr;
    getline(cin, choiceStr);

//I am aware there might be a much better way to do this but it works for now, come back later if need be

    if (all_of(choiceStr.begin(), choiceStr.end(), ::isdigit)) {
        int choice = stoi(choiceStr);
        if (choice >= 1 && choice <= 25) {
            if (choice == 1) {
                C1H1.aboutHotel();
            } else if (choice == 2) {
                C1H2.aboutHotel();
            } else if (choice == 3) {
                C1H3.aboutHotel();
            } else if (choice == 4) {
                C1H4.aboutHotel();
            } else if (choice == 5) {
                C1H5.aboutHotel();
            } else if (choice == 6) {
                C2H1.aboutHotel();
            } else if (choice == 7) {
                C2H2.aboutHotel();
            } else if (choice == 8) {
                C2H3.aboutHotel();
            } else if (choice == 9) {
                C2H4.aboutHotel();
            } else if (choice == 10) {
                C2H5.aboutHotel();
            } else if (choice == 11) {
                C3H1.aboutHotel();
            } else if (choice == 12) {
                C3H2.aboutHotel();
            } else if (choice == 13) {
                C3H3.aboutHotel();
            } else if (choice == 14) {
                C3H4.aboutHotel();
            } else if (choice == 15) {
                C3H5.aboutHotel();
            } else if (choice == 16) {
                C4H1.aboutHotel();
            } else if (choice == 17) {
                C4H2.aboutHotel();
            } else if (choice == 18) {
                C4H3.aboutHotel();
            } else if (choice == 19) {
                C4H4.aboutHotel();
            } else if (choice == 20) {
                C4H5.aboutHotel();
            } else if (choice == 21) {
                C5H1.aboutHotel();
            } else if (choice == 22) {
                C5H2.aboutHotel();
            } else if (choice == 23) {
                viewCities();
            } else if (choice == 24) {
                menu();
            } else {
                exit(0);
            }
        } else {
            cout << "Invalid choice. Please enter a valid number between 1 and 25." << endl;
        }
    } else {
        cout << "Invalid choice. Please enter a valid number." << endl;
    }
}

